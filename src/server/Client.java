package server;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private Socket sock;
    private PrintWriter out;
    private BufferedReader in;

    public Client(String host, int port) throws IOException {
        sock = new Socket(host, port);
        out = new PrintWriter(sock.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    }

    public JSONObject send(String expr) throws IOException, ParseException {
        JSONObject message = new JSONObject();
        message.put("expr", expr);
        message.put("type", "eval");
        message.put("key", "wasd");

        out.println(message.toJSONString());
        String data =  in.readLine();
        JSONParser parser = new JSONParser();
        JSONObject response = (JSONObject) parser.parse(data);
        return response;
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        sock.close();
    }
    public static void pressAnyKeyToContinue(){
        System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        }
        catch (Exception e)
        {}
    }
    public static void main(String[] args) throws IOException, ParseException {
        Client client = new Client("127.0.0.1", 5000);
        JSONObject response = client.send("2+2");
        System.out.println(response);

        pressAnyKeyToContinue();
        JSONObject response2 = client.send("(-2+3)*6/2");
        System.out.println(response2);
        client.stop();
    }
}
