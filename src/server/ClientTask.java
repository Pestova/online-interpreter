package server;

import interpreter.Interpreter;
import interpreter.Lexer;
import interpreter.Parser;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientTask implements Runnable{
    private Socket clientSock;
    private PrintWriter out;
    private BufferedReader in;

    public ClientTask(Socket client) throws IOException {
        this.clientSock = client;
        out = new PrintWriter(clientSock.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSock.getInputStream()));
    }

    @Override
    public void run() {
        JSONParser parser = new JSONParser();
        System.out.println("Opened connection to " + clientSock.getRemoteSocketAddress());
        while (!clientSock.isOutputShutdown()) {
            String data = null;
            JSONObject answer = new JSONObject();
            try {
                data = in.readLine();
                if (data == null){
                    break;
                }
                JSONObject message = (JSONObject) parser.parse(data);
                if (message.containsKey("expr") && message.get("key").equals("wasd")) {
                    Lexer lexer = new Lexer((String) message.get("expr"));
                    try {
                        Parser exprParser = new Parser(lexer);
                        Interpreter interp = new Interpreter(exprParser);
                        float result = interp.interpret();
                        answer.put("result", result);
                    } catch (Exception e) {
                        answer.put("error", e.getMessage());
                    }
                } else {
                    answer.put("error", "Expression is not found");
                }
            } catch (IOException | ParseException e) {
                answer.put("error", "JSON is not valid");
            }
            out.println(answer.toJSONString());
        }
        System.out.println("Close connection "+ clientSock.getRemoteSocketAddress());
        try {
            in.close();
            out.close();
            clientSock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
