package logging;

import java.io.PrintStream;

public class ConsoleHandler extends BaseHandler{

    public ConsoleHandler(BaseFormatter formatter) {
        super(formatter);
    }

    @Override
    public void log(Message message) {
        PrintStream stream;
        if (message.type.equals(Message.MType.CRITICAL)){
            stream = System.err;
        }
        else{
            stream = System.out;
        }
        stream.println(formatter.format(message));
    }

    public static void main(String[] args){
        ConsoleHandler handler = new ConsoleHandler(new StringFormatter());
        handler.log(new Message("Critical",
                Message.MType.CRITICAL));
    }

}
