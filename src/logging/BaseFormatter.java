package logging;

public abstract class BaseFormatter {

    protected String fmt;

    public abstract String format(Message message);
}
