package logging;

import java.util.Date;

class Message {
    public enum MType {INFO, WARNING, CRITICAL}

    public Date date;
    public String message;
    public MType type;
    public String callerClass;
    public String callerMethod;
    public int line;

    public Message(String message, MType type){
        date = new Date();
        this.message = message;
        this.type = type;
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        StackTraceElement top = stack[2];
        callerClass = top.getClassName();
        callerMethod = top.getMethodName();
        line = top.getLineNumber();
    }

    public static void main(String[] args){
        Message message = new Message("Critical",
                MType.CRITICAL);
        System.out.println(message.date);
        System.out.println(message.callerMethod);
    }

}
