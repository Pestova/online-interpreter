package logging;

public class StringFormatter  extends BaseFormatter{

    public StringFormatter(){
        fmt = "%s %s %s %d %s %s";
    }

    @Override
    public String format(Message message) {
        return String.format(fmt, message.date,
                message.callerClass, message.callerMethod,
                message.line, message.type, message.message);
    }

    public static void main(String[] args){
        Message msg = new Message("Message",
                Message.MType.INFO);
        System.out.println(new StringFormatter().format(msg));
    }

}
