package logging;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileHandler extends BaseHandler {
    private File file;

    public FileHandler(BaseFormatter formatter, String path) {
        super(formatter);
        file = new File(path);
        if (!file.isFile()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                Date date = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH:mm");
                try {
                    file = File.createTempFile(format.format(date), ".log");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void log(Message message) {

    }
}
