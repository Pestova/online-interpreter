package logging;

public abstract class BaseHandler {
    protected BaseFormatter formatter;

    public BaseHandler(BaseFormatter formatter){
        this.formatter = formatter;
    }

    public abstract void log(Message message);


}
